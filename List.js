import React, { Component } from 'react'
import { Text, View, FlatList, StyleSheet } from 'react-native'


const Item = ({ title, description }) => (
    <View>
        <Text>{title} </Text>
        <Text>{description} </Text>
    </View>
);

class List extends Component {
    data = [
        { id: 1, title: "Black", description: "eqwed" },
        { id: 2, title: "Black1", description: "e   wfc3r   2" },
        { id: 3, title: "Black2", description: "wqc qcw" },
        { id: 4, title: "Black43", description: "ertd egrs" },
        { id: 5, title: "Black5", description: "aac sdc" }
    ];

    renderItem = ({ item }) => (
        <Item title={item.title} description={item.description} />
    );


    render() {
        return (
            <View >
                <FlatList
                    data={this.data}
                    renderItem={this.renderItem}
                    // keyExtractor={(item) => item.id}
                />
            </View >
        );
    };
}

export default List

// const styles = StyleSheet.create({
//     container: {
//         padding: 10,
//         marginTop: 3,
//         backgroundColor: '#d9f9b1',
//         alignItems: 'center',
//     },
//     text: {
//         color: '#4f603c'
//     }
// })