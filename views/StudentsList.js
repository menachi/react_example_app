import React, { Component } from 'react'
import { Text, View, Image, TextInput, StyleSheet, TouchableOpacity, Button, FlatList } from 'react-native'



class StudentsList extends Component {
    state = {
        data: [
            { text: 'first  aaaaaa w 2', id: 1 }, { text: 'second 1', id: 2 }, { text: 'third', id: 3 },
            { text: 'first  2', id: 1 }, { text: 'second 1', id: 2 }, { text: 'third', id: 3 },
            { text: 'first  2', id: 1 }, { text: 'second 1', id: 2 }, { text: 'third', id: 3 },
            { text: 'first  2', id: 1 }, { text: 'second 1', id: 2 }, { text: 'third', id: 3 },
            { text: 'first  2', id: 1 }, { text: 'second 1', id: 2 }, { text: 'third', id: 3 },
            { text: 'first  2', id: 1 }, { text: 'second 1', id: 2 }, { text: 'third', id: 3 },
        ]
    }

    onRowSelected(itemData){
        console.log('item was clicked ' + itemData.item.text)
    }

    renderSingleItem = itemData => {
        console.log('renderSingleItem ' + itemData.item.toString())
        console.log('renderSingleItem ' + itemData.item.text)
        return (
            <TouchableOpacity onPress={()=>this.onRowSelected(itemData)}>
                <View style={{ flexDirection: 'row', borderColor: '#009688', borderWidth: 1 }}>
                    <View style={{ margin: 5 }}>
                        <Image style={{ width: '100%', height: 100, aspectRatio: 1 }}
                            source={require('../resources/avatar.png')} />
                    </View>
                    <View style={{}}>
                        <Text style={{
                            flex: 1, textAlignVertical: 'center'
                            , paddingTop: 5, fontSize: 24
                        }}>{itemData.item.text}</Text>
                        <Text style={{
                            flex: 1, textAlignVertical: 'center'
                            , paddingBottom: 5, fontSize: 20
                        }}>{itemData.item.text}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    render() {
        return (
            <FlatList
                data={this.state.data}
                renderItem={item => this.renderSingleItem(item)}
                keyExtractor={item => item.id.toString()}
            />
        )
    }
}



export default StudentsList
