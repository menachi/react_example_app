import React, { Component } from 'react'
import { Text, View, Image, TextInput, StyleSheet, TouchableOpacity, Button } from 'react-native'

class StudentDetails extends Component {
    state = { StName:'' }

    onSave() {
        console.log('onPress' + this.state.StName)
    }
    onCancel() {
        console.log('onPress' + this.state.StName)
    }
    render() {
        return (
            <View style={{ padding: 10 }}>
                <View style={styles.container}>
                    <Image style={styles.image}
                        source={require('../resources/avatar.png')} />
                </View>
                <View style={styles.row_container}>
                    <Text style={styles.text}>Name:</Text>
                    <TextInput
                        style={styles.input}
                        placeholder="This is a place holder!"
                        onChangeText={name => this.setState({StName:name})}
                        />
                </View>
                <View style={styles.row_container}>
                    <Text style={styles.text}>ID:</Text>
                    <TextInput style={styles.input}></TextInput>
                </View>
                <View style={styles.row_container}>
                    <Text style={styles.text}>Phone:</Text>
                    <TextInput style={styles.input}></TextInput>
                </View>
                <View style={styles.row_container}>
                    <View style={{ flex: 1, margin: 5 }}>
                        <TouchableOpacity onPress={this.onCancel.bind(this)} style={styles.appButtonContainer}>
                            <Text style={styles.appButtonText}>CANCEL</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ flex: 1, margin: 5 }}>
                        <TouchableOpacity onPress={this.onSave.bind(this)} style={styles.appButtonContainer}>
                            <Text style={styles.appButtonText}>SAVE</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: 5,
        alignItems: 'center',
    },
    row_container: {
        marginTop: 5,
        flexDirection: 'row',
        alignItems: 'center'
    },
    text: {
        color: '#4f603c',
        fontSize: 25,
        flexShrink: 1,
    },
    image: {
        width: '100%',
        height: 200,
        aspectRatio: 1,
    },
    input: {
        marginTop: 5,
        marginBottom: 5,
        marginLeft: 5,
        borderColor: '#009688',
        borderWidth: 1,
        fontSize: 25,
        flex: 1
    },
    appButtonContainer: {
        elevation: 8,
        backgroundColor: "#009688",
        borderRadius: 10,
        paddingVertical: 10,
        paddingHorizontal: 12,
    },
    appButtonText: {
        fontSize: 18,
        color: "#fff",
        fontWeight: "bold",
        alignSelf: "center",
        textTransform: "uppercase"
    }
})

export default StudentDetails

